## Разработка ИИ в облачной среде с помощью веб-сервисов Amazon
Акции FANG (Facebook, Amazon, Netflix и Google) неудержимо растут в цене в последние несколько лет. Акции одного только Amazon выросли на 300% за период с марта 2015-го по март 2018 г. Netflix также работает на основе АWS. С точки зрения карьерного роста динамика облака АWS очень неплоха и там крутятся огромные деньги . Понимание специфики работы этой платформы и предоставляемых ею сервисов критически важно для успеха в ближайшем будущем многих приложений ИИ.

Из такого массового перемещения денежных масс можно сделать вывод,
что облака не только воцарились надолго, но и меняют базовую парадигму разработки программного обеспечения. В часстности, АWS сделал ставку на бессерверные технологии. Жемчужина этого стека - технология АWS Lambda, позволяющая выполнять функции на множестве языков - Go, Python, Java, С# и Node - в виде событий внутри большей экосистемы. А облако можно рассматривать как новую операционную истему.

Язык Python известен жесткими ограничениями по масштабируемости в силу самой своей природы. Несмотря на вдохновенные ошибочные, но почти убедителъные аргументы в пользу того, что глобальная блокировка интерпретатора (global interpreter lock, GIЦ, как и производительность Python, неважны, на самом деле на практике при больших масштабах они имеют значение. Именно те черты Python, которые упрощают его использование, исторически оказываются проклятием для его производительности. В частности, GIL, по сути, блокирует возможность эффективного распараллеливания при масштабных вычислениях, по сравнению с другими языками программирования, такими как Jаvа. Конечно, существуют способы обхода подобных ограничений на целевой Linuх-машине, но это часто приводит к колоссальным тратам времени на неуклюжее переписывание принципов конкурентности языка Erlang на Python или к простаиванию ядер процессора.

При использовании технологии АWS Lambda этот недостаток Python теряет значение, поскольку роль операционной системы играет сам АWS. Вместо того чтобы применять потоки выполнения или процессы для распараллеливания кода, разработчик облачных сервисов может воспользоваться SNS, SQS, Lambda и другими технологиями на основе стандартных блоков.
Эти примитивы далее занимают место потоков выполнения, процессов
и других парадигм традиционных операционных систем. Дальнейшее подтверждение проблем с масштабированием Python в операционной системе Linux связано с углубленными исследованиями предполагаемых широких возможностей масштабирования проектов Python.

Если копнуть поглубже, вы обнаружите, что фактически всю работу выполняет, скажем, RabbltMQ, и/или Celery (написанные на Erlang), или Nginx, написанный на высокооптимизированном С. Но не слишком увлекайтесь Erlang (я руководил компанией, интенсивно его использовавшей) - нанять кого-то, кто пишет на этом языке, практически нереально. Язык Go понемногу начинает решать те же проблемы с масштабированием, а нанять разработчиков на Go намного проще. С другой стороны, возможно, лучше всего перенести реализацию конкурентности вглубь облачной операционной системы, сняв ее со своих плеч. Тогда увольнение вашего бесценного разработчика на Erlang или Go не приведет к банкротству компании.

К счастью, однако, при использовании бессерверной технологии недостатки Python при работе в операционной системе Linux внезапно оказываются неважны. Я столкнулся с хорошим примером этого во время работы инженером в занимавшейся большими данными компании Loggly. Мы пытались написать высокопроизводительную асинхронную систему ввода и обработки журналов на Python, и да, при работе на одном ядре она демонстрировала отличные результаты, от шести до восьми тысяч записей в секунду. Но проблема заключалась в простое остальных ядер, и масштабирование этого асинхронного средства сбора данных на несколько ядер не оправдывало затрат. С помощью создания бессерверных компонентов из А WS, однако, написание всей системы на Python - прекрасная идея в силу естественной масштабируемости данной платформы.

Но не только эти новые облачные операционные системы имеет смысл учитывать. Многие такие технологии, как веб-фреймворки, представляют собой абстракции, построенные на абстракциях из далекого прошлого. Реляционные базы данных были придуманы в 1970-х, и это добротная технология, но в начале 2000-х разработчики веб-фреймворков нагромоздили поверх этой, возникшей в эру персональных компьютеров и ЦОД, технологии множество веб-фреймворков с помощью средств объектно-реляционного отображения и утилит генерации кода. Создание веб-фреймворков становится, чуть ли не преднамеренно, инвестицией в устаревший образ мышления. Безусловно, они способны решать свои задачи и обладают большими возможностями, но за ними ли будущее, особенно с учетом масштабных проектов ИИ? Не думаю.

Бессерверные технологии представляют совершенно иной стиль мышления. Они предлагают автоматическое масштабирование баз данных наряду с гибкостью и эффективностью управления схемами. Вместо запуска клиентской веб-части, например Apache или Nginx, передающих запросы прикладному коду, здесь применяются серверы приложений без сохранения состояния, запускаемые только при поступлении событий.

Возникает, однако, проблема сложности. Поскольку сложность приложений машинного обучения и ИИ растет, приходится чем-то жертвовать ради ее снижения. Один из способов уменьшения сложности - отказ от серверов, а значит, и затрат на их сопровождение. Подобное может также означать, что пришла пора сказать rm -rf1 традиционным веб-фреймворкам. Однако это дело не ближайших дней, так что в данной главе мы рассмотрим традиционное веб-приложение, Flask, но с "привкусом" облачной операционной системы. В других главах вы найдете примеры чисто бессерверной архитектуры, а именно с использованием АWS Chalice.

## Создание решений дополненной и виртуальной реальностей на основе AWS
Работая в киноиндустрии и в Caltech, я начал высоко ценить мощные файловые серверы на Linux, установленные на всех рабочих станциях компании. Единая центральная точка подключения для настройки операционной системы, распределения данных и совместного использования дисковых операций ввода/вывода для тысяч машин и пользователей предоставляет большие возможности.

Мало кому известно, что множество кинокомпаний включено в список обладателей 500 самых мощных суперкомпьютеров, причем уже многие годы (https://www.topSOO.org/news/new-zealand-to-join-petaflop-club/). Причина состоит в том, что системы визуализации, связанные с высокопроизводительными централизованными файловыми серверами, используют колоссальные вычислительные ресурсы и ресурсы дискового ввода/вывода. В кинокомпании Weta Digital, когда я работал там над фильмом «Аватар» в Новой Зеландии в 2009 г. (https://www.geek.com/chips/the-computing-power-that-created-avatar-1031232/), было более 40 ООО процессоров с 104 Тбайт памяти. Они обрабатывали ежедневно до 1,4 млн задач, так что многие ветераны кинопромышленности снисходительно улыбаются, слыша про нынешние рабочие нагрузки Spark и Hadoop.

Весь смысл упоминания об этом (помимо того, чтобы сказать новичкам в больших данных: «Прочь с моей лужайки») - показать, насколько прекрасен централизованный файловый сервер при масштабных вычислениях. Исторически, однако, так сложилось, что эти «Феррари» среди файловых серверов требуют целой команды «механиков» для поддержания работоспособности. А с наступлением эры облачных вычислений внезапно для получения такого файлового сервера оказывается достаточно выбрать его и щелкнуть кнопкой мыши.

## Компьютерное зрение: создание конвейеров AR/VR с помощью EFS и Flask
В AWS есть адаптивная файловая система (Elastic File System, EFS) - сервис, служащий именно для получения превосходного файлового сервера буквально за один щелчок кнопкой мыши. В прошлом мне случалось использовать этот сервис, в частности, для создания в АWS централизованного файлового сервера для конвейера компьютерного зрения на основе виртуальной реальности (VR). Все ресурсы, код и созданные артефакты хранились в EFS. Блоки камер могут насчитывать 48 или 72 камеры, каждая из которых генериру т крупные кадры, поступающие в дальнейшемдля обработки в алгоритм компоновки сцен виртуальной реальности.

![](./img/img_1.png)

Один yезаметный, но существенный нюанс состоит в серьезном упрощении развертывания кода приложений Python, поскольку можно создать точки подключения EFS для каждой из сред, скажем, разработки (DEV), предэксплуатационного тестирования (STAGE) и промышленной эксплуатации (PRODUCТION). После этого для развертывания достаточно будет вы полнить удаленную инхронизацию (rsync) кода между сервером сборки и точкой подключения EFS, что можно сделать за доли секунды в зависимо сти от ветки: допустим, точка подключения DEV представляет собой основную ветку, а точка подключения STAGE - ветку пр дэксплуатационного тестирования и т. д. При этом фреймворк Flask обеспечивает наличие на диске свежей версии кода, благодаря чему развертывание становится тривиальной задачей. 

![](./img/img_2.png)

Возможность использования при необходимости бессерверной технологии весьма упрощает задачу, кроме того, EFS вместе с Flask - мощный инструмент для создания ИИ продуктов всех мастей. В приведенных примерах речь идет о конвейерах компьютерного зрения/VR/ AR, но EFS может пригодиться и при инженерии данных для обычного машинного обучения.

Наконец, я уверен в работоспособности этой архитектуры еще и потому, что создавал ее с нуля для компании, занимающейся AR/VR, и она оказалась настолько популярна, что мы всего за несколько месяцев израсходовали $100 ООО кредита от AWS, запуская гигантские задания на многих сотнях узлов. Иногда успех означает изрядную дыру в кармане.

## Создание конвейера инженерии данныхс помощью EFS, Flask и Pandas
При создании конвейера ИИ промышленного класса основная трудность часто заключается в инженерии данных. В следующем подразделе будет подробно описано, как можно заложить начала API для промышленной эксплуатации в компаниях вроде Netflix, АWS или стартапах с миллиардной капитализацией. Командам исследователей данных часто приходится создавать библиотеки и сервисы для упрощения работы с данными на используемой их компанией платформе.

В этом примере мы выполним в качестве подтверждения работоспособности концепции агрегирование данных в формате CSV. Воплощающий REST API будет принимать на входе файл в формате CSV, столбец, по которому необходимо группировать данные, и столбец, в котором будет возвращаться результат. Отмечу также реалистичность этого примера: он включает реализацию всех мелких нюансов, таких как документирование API, тестирование, непрерывная поставка, плагины, а также оценку производительности.

Входные данные для нашей задачи выглядят следующим образом:

    first_name,last_name,count
    chuck,norris,10
    kristen,norris,17
    john,lee,3
    sam,mcgregor,15
    john,mcgregor,19

После обработки с помощью API они приобретут такой вид:

    norris,27
    lее,З
    mcgregor,34

Код проекта можно найти здесь: https://github.com/noahgift/pai-aws. А поскольку сборочные файлы и виртуальные среды мы подробно рассматриваем в других главах, перейдем прямо к коду. Данный проект состоит из пяти основных частей: приложения Flask, библиотеки nlib, блокнотов, тестов и утилиты командной строки.

## Приложение Flask

Приложение Flask состоит из трех компонентов: каталога static, содержащего файл favicon.ico; каталога templates, содержащего файл index. html; и основного веб-приложения, насчитывающего около 150 строк кода. Вот пошаговый разбор кода основного приложения Flask.
В первом разделе файла производится импорт Flask и его расширения flasgger (генератора документации на основе API фреймворка Swagger; https://github.com/rochacbruno/flasgger), а также описываются компоненты журналирования и приложение Flask:

    import os
    import Ьаsеб4
    import sys
    from io import BytesIO
    from flask import Flask
    from flask import send_from_directory
    from flask import request
    from flask_api import status
    from flasgger import Swagger
    from flask import redirect
    from flask import jsonify
    from sensiЫe.loginit import logger
    from nlib import csvops
    from nlib import utils
    log = logger(~name~)
    арр = Flask(~name~)
    Swagger(app)

Создаем вспомогательную функцию для декодирования данных, находящихся в формате Base64:

    def _b64decode_helper(request_object):
    	"""Returns base64 decoded data and size of encoded data"""

    	size=sys.getsizeof(request_object.data)
    	decode_msg = "Decoding data of size: {size}".format(size=size)
    	log.info(decode_msg)
    	decoded_data = BytesIO(base64.b64decode(request.data))
   	 return decoded_data, size

Далее мы создаем несколько, по сути, шаблонных маршрутов, предназначенных для выдачи файла favicon.ico и перенаправления к основной документации:

    @app.route("/")
    def home():
        """/ Route will redirect to API Docs: /apidocs"""
        return redirect("/apidocs")


    @app.route("/favicon.ico")
    def favicon():
        """The Favicon"""
        return send_from_directory(os.path.join(app.root_path, 'static'),
                        'favicon.ico', mimetype='image/vnd.microsoft.icon')

Ситуация оказывается интереснее в случае маршрута /api/fuпcs. Там перечисляются устанавливаемые плагины, которые могут представлять собой пользовательские алгоритмы. Я опишу их подробнее в подразделе,тпосвященном библиотеке.

    def list_apply_funcs():
        """Return a list of appliable functions
            GET /api/funcs
            ---
            responses:
                200:
                    description: Returns list of appliable functions.
        """
        appliable_list = utils.appliable_functions()
        return jsonify({"funcs":appliable_list})

В этом разделе создается маршрут для группировки, она содержит подробную документацию в виде docstring, что позволяет динамически создать документацию на основе API swagger:

    @app.route('/api/<groupbyop>', methods = ['PUT'])
    def csv_aggregate_columns(groupbyop):
        """Aggregate column in an uploaded csv
        
        ---
            consumes:  application/json
            parameters:
                -   in: path
                    name:  Appliable Function (i.e.  npsum, npmedian)
                    type:  string
                    required: true
                    description:  appliable function, which must be registered (check /api/funcs)
                -   in: query
                    name: column
                    type: string
                    description:  The column to process in an aggregation
                    required:  True
                -   in: query 
                    name: group_by
                    type: string
                    description:  The column to group_by in an aggregation
                    required:  True
                -   in: header
                    name:  Content-Type
                    type:  string
                    description:  Requires "Content-Type:application/json" to be set
                    required:  True
                -   in: body
                    name: payload
                    type:  string
                    description:  base64 encoded csv file
                    required: True
            responses:
                200:
                    description: Returns an aggregated CSV.
        """

Наконец, далее создается ядро вызова API. Обратите внимание, что здесь решается множество запутанных практических задач, например проверка правильного типа содержимого, поиск конкретного метода НТТР, журналирование динамически загружаемых плагинов, а также возвращение JSОN-ответа с кодом состояния 200 в случае, если все в порядке, и других кодов состояния, если нет.

        content_type = request.headers.get('Content-Type')
        content_type_log_msg = "Content-Type is set to:  {content_type}".\
            format(content_type=content_type)
        log.info(content_type_log_msg)
        if not content_type == "application/json":
            wrong_method_log_msg =\
                 "Wrong Content-Type in request: {content_type} sent, but requires application/json".\
                format(content_type=content_type)
            log.info(wrong_method_log_msg)
            return jsonify({"content_type": content_type, 
                    "error_msg": wrong_method_log_msg}), status.HTTP_415_UNSUPPORTED_MEDIA_TYPE
    
        #Parse Query Parameters and Retrieve Values
        query_string = request.query_string
        query_string_msg = "Request Query String: {query_string}".format(query_string=query_string)
        log.info(query_string_msg)
        column = request.args.get("column")
        group_by = request.args.get("group_by")

        #Query Parameter logging and handling
        query_parameters_log_msg = "column: [{column}] and group_by: [{group_by}] Query Parameter values".\
            format(column=column, group_by=group_by) 
        log.info(query_parameters_log_msg)
        if not column or not group_by:
            error_msg = "Query Parameter column or group_by not set"
            log.info(error_msg)
            return jsonify({"column": column, "group_by": group_by, 
                    "error_msg": error_msg}), status.HTTP_400_BAD_REQUEST

        #Load Plugins and grab correct one
        plugins = utils.plugins_map()
        appliable_func = plugins[groupbyop]

        #Unpack data and operate on it
        data,_ = _b64decode_helper(request)
        #Returns Pandas Series
        res = csvops.group_by_operations(data, 
            groupby_column_name=group_by, apply_column_name=column, func=appliable_func)
        log.info(res)
        return res.to_json(), status.HTTP_200_OK

Следующий фрагмент кода устанавливает такие флаги, как debug, и включает шаблонный код для запуска приложения Flask в виде сценария:

    if __name__ == "__main__": # pragma: no cover
        log.info("START Flask")
        app.debug = True
        app.run(host='0.0.0.0', port=5001)
        log.info("SHUTDOWN Flask")

Далее я создал следующий сборочный файл для запуска приложения:

    (.pia-aws)-+ pai-aws git:(master) make start-api
    #Устанавливает переменную среды PYTHONPATH так, чтобы она указывала
    #на каталог, расположенный на один уровень выше
    #В промышленной эксплуатации будет иначе
    cd flask_app && PYTHONPATH=" .. " python web.py
    2018-03-17 19:14:59,807 - _main_ - INFO - START Flask
    * Running on http://0.0.0.0:5001/ (Press CTRL+C to quit)
    * Restarting with stat 2018-03-17 19:15:00,475 - _main_ - INFO - START Flask
    * Debugger is active!
    * Debugger PIN: 171-594-84

#### Библиотека и плагины
Внутри каталога nlib находится четыре фaйлa: init.py, applicble.py, csvops.py и utils.ру . Вот построчный разбор каждого из них.

Файл init.ру очень прост, он содержит пер менную для версии:

    _version_ = 0.1

Далее идет файл utils. ру - загрузчик плагинов, выбирающий «допустимую» функцию из файла applicble.py:

    import importlib

    from sensible.loginit import logger

    log = logger(__name__)

    def appliable_functions():
        """Returns a list of appliable functions to be used in GroupBy Operations"""

        from . import appliable
        module_items = dir(appliable)
        #Filter out special items __
        func_list = list(filter(lambda x: not x.startswith("__"), module_items))
        return func_list


    def plugins_map():
        """Create a dictionary of callable functions
        In [2]: plugins = utils.plugins_map()
        2017-06-22 10:34:25,312 - nlib.utils - INFO - Loading appliable functions/plugins: npmedian
        2017-06-22 10:34:25,312 - nlib.utils - INFO - Loading appliable functions/plugins: npsum
        2017-06-22 10:34:25,312 - nlib.utils - INFO - Loading appliable functions/plugins: numpy
        2017-06-22 10:34:25,312 - nlib.utils - INFO - Loading appliable functions/plugins: tanimoto
        In [3]: plugins
        Out[3]: 
        {'npmedian': <function nlib.appliable.npmedian>,
        'npsum': <function nlib.appliable.npsum>,
        'numpy': <module 'numpy' from '/Users/noahgift/.nflixenv/lib/python3.6/site-packages/numpy/__init__.py'>,
        'tanimoto': <function nlib.appliable.tanimoto>}
        In [4]: plugins['npmedian']([1,3])
        Out[4]: 2.0
        """

        plugins = {}
        funcs = appliable_functions()
        for func in funcs:
            plugin_load_msg = "Loading appliable functions/plugins: {func}".format(func=func)
            log.info(plugin_load_msg)
            plugins[func] = getattr(importlib.import_module("nlib.appliable"), func)
        return plugins


В файле applicble.ру можно создавать пользовательские функции. Эти функции применяются к столбцу объекта DataFrame библиотеки Pandas, их можно адаптировать для любых действий, которые только выполняются над столбцом:

    import numpy


    def tanimoto(list1, list2):
        """tanimoto coefficient
        In [2]: list2=['39229', '31995', '32015']
        In [3]: list1=['31936', '35989', '27489', '39229', '15468', '31993', '26478']
        In [4]: tanimoto(list1,list2)
        Out[4]: 0.1111111111111111
        Uses intersection of two sets to determine numerical score
        """

        intersection = set(list1).intersection(set(list2))
        return float(len(intersection))/(len(list1) + len(list2) - len(intersection))

    def npsum(x):
        """Numpy Library Sum"""

        return numpy.sum(x)

    def npmedian(x):
        """Numpy Library Median"""

        return numpy.median(x)

Наконец, модуль cvops отвечает за ввод СSV-файла и операции над ним, как показано ниже:

    from sensible.loginit import logger
    import pandas as pd

    log = logger(__name__)
    log.debug("imported csvops module")

    def ingest_csv(data):
        """Ingests a CSV using Pandas CSV I/O"""

        df = pd.read_csv(data)
        return df

    def list_csv_column_names(data):
        """Returns a list of column names from csv"""

        df = ingest_csv(data)
        colnames = list(df.columns.values)
        colnames_msg = "Column Names: {colnames}".format(colnames=colnames)
        log.info(colnames_msg)
        return colnames

    def aggregate_column_name(data, groupby_column_name, apply_column_name):
        """Returns aggregated results of csv by column name as json"""

        df = ingest_csv(data)
        res = df.groupby(groupby_column_name)[apply_column_name].sum()
        return res

    def group_by_operations(data, groupby_column_name, apply_column_name, func):
        """
        Allows a groupby operation to take arbitrary functions

        In [14]: res_sum = group_by_operations(data=data, groupby_column_name="last_name", columns="count", func=npsum)
        In [15]: res_sum
        Out[15]:
        last_name
        eagle    34
        lee       3
        smith    27
        Name: count, dtype: int64
        """

        df = ingest_csv(data)
        grouped = df.groupby(groupby_column_name)[apply_column_name] #GroupBy with filter to specific column(s)
        applied_data = grouped.apply(func)
        return applied_data


## Оценка производительности и тестирование API.
При создании API для реальной промышленной эксплуатации было бы в корне неверно не оценить его производительность до начала работы. Вот так это можно сделать с помощью команды сборочного файла:

    (.pai-aws) ➜  pai-aws git:(inperson-interview) ✗ make benchmark-web
    #very simple benchmark of api
    ab -n 1000 -c 100 -T 'application/json' -u ext/input_base64.txt http://0.0.0.0:5001/api/aggregate\?column=count\&group_by=last_name
    This is ApacheBench, Version 2.3 <$Revision: 1757674 $>
    Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
    Licensed to The Apache Software Foundation, http://www.apache.org/

    Benchmarking 0.0.0.0 (be patient)
    Completed 1000 requests
    Finished 1000 requests

    Server Software:          Werkzeug/0.12.2
    Server Hostname:        0.0.0.0
    Server Port:                  5001

    Document Path:            /api/aggregate?column=count&group_by=last_name
    Document Length:        154 bytes

    Concurrency Level:      100
    Time taken for tests:    7.657 seconds
    Complete requests:     1000
    Failed requests:           0
    Total transferred:         309000 bytes
    Total body sent:           308000
    HTML transferred:       154000 bytes
    Requests per second: 130.60 [#/sec] (mean)
    Time per request:       765.716 [ms] (mean)
    Time per request:       7.657 [ms] (mean, across all concurrent requests)
    Transfer rate:              39.41 [Kbytes/sec] received
                                       39.28 kb/s sent
                                       78.69 kb/s total

В данном случае производительность приложения вполне удовлетворительна, как и его масштабируемость при использовании адаптивного балансировщика нагрузки (elastic load balancer, ELB) с несколькими узлами Nginx. Однако это все же пример того, что Python, будучи прекрасным языком написания кода с большими возможностями, все же сильно уступает в смысле производительности таким языкам, как С++, .Java, С# и Go. Приложения, написанные на Erlang или Go, зачастую выполняют подобные функции, получая тысячи запросов в секунду.

В данном случае, однако, скорость работы созданного приложения и конкретный сценарий использования соотносятся вполне оптимально. В числе идей возможных доработок в версии 2 - переключение на АWS Chalice и применение Spark и/или Redis для кэширования запросов и сохранения результатов в оперативной памяти. Обратите внимание, что в АWS Chalice есть возможность кэширования запросов API по умолчанию, благодаря чему добавление дополнительных слоев кэширования не представляет сложности.

## Резюме

АWS - весьма разумный выбор в качестве фундамента технологических решений компании. Достаточно посмотреть на рыночную капитализацию компании Amazon, чтобы понять, что она еще довольно долго будет вводить новшества и снижать цены. Достижения Amazon в области бессерверных технологий весьма впечатляют.

Не волнуйтесь о возможной подчиненности определенному поставщику сервисов, ведь использование Erlang или DigitalOcean в ЦОД не означает зависимости от поставщика. Истинная зависимость здесь - от маленькой команды разработчиков со своими странностями и системных администраторов.
В данной главе были продемонстрированы вполне реалистичные API и решения, в основе которых лежат задачи, выполненные мной при консультациях по АWS. Многие идеи из других глав этой книги можно связать с идеями настоящей главы, и такое их содружество вполне может превратиться в решение для промышленной эксплуатации.

### Source: "Ной Гифт - Прагматичный ИИ. Машинное обучение и облачные технологии."
